CurrencyExchangeBundle providing insight on currency exchange rates on various bank exchange service providers. Bundle is built on top of Symfony 3.1.2 ver.

# Service Providers #

Currently supported service providers are as follows

* European Central Bank (providing it's exchange data via it's own services, but for better abstraction bundle uses http://fixer.io/ API end point(s) for retrieving data.
* CurrencyLayer API provider
* OpenExchange API provider.


# Caching #

Data caching support is added via Caching class implementation. Currently there is an option only to cache it to the database. Caching is recreated every 3 hours.

# Setup #

1. Create Database and set its configs in 'root'\app\config\parameters.yml
2. Populate database with tables used for caching using command $ php bin/console doctrine:schema:create
3. run composer install to setup any external dependencies for this bundle

# Commands #

There are useful symfony commands for retrieving relevant data via CLI such as

* php bin/console currency:rate:best [currency_from] [currency_to] (currency codes must follow ISO 4217 standard and must strictly be no less or more than 3 character long.

* php bin/console currency:rates [currency_from] [currency_to] provides all available rates for given currency pair, must follow the same standard as command above.

# Dependencies #

1. [Link CiRestClientBundle](Link https://github.com/CircleOfNice/CiRestClientBundle) used for RESTful client calls to various currency exchange service providers.

# Usage #
1. Retrieve service from global container by calling.  
```
#!php

$rate = $this->get('app.exchange_rate');

// $from - currency you are selling, $to currency you are buying
$rate = $service->get($from, $to);
```
method above returns single key value pair array containing best exchange rate and Bank object providing such exchange rate.