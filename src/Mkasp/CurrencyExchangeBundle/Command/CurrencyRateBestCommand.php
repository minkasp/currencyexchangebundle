<?php

namespace Mkasp\CurrencyExchangeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CurrencyRateBestCommand extends ContainerAwareCommand
{
    /** @var null | ExchangeRateRepository  */
    private $exchangeRepository = null;

    protected function configure()
    {
        $this
            ->setName('currency:rate:best')
            ->setDescription('Retrieves best currency exchange rate for given currency pair')
            ->addArgument('currencyFrom', InputArgument::REQUIRED, 'Currency you\'re selling')
            ->addArgument('currencyTo', InputArgument::REQUIRED, 'Currency you\'re buying');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $this->exchangeRepository = $em
            ->getRepository('Mkasp\CurrencyExchangeBundle\Entity\ExchangeRate');

        $from = strtoupper($input->getArgument('currencyFrom'));
        $to = strtoupper($input->getArgument('currencyTo'));
        $rate = $this->exchangeRepository->getBestRate($from, $to);

        if ($rate == null) {
            $io->warning("Unfortunately, no exchange rate have been found for ".$from.'/'.$to. ' currency pair.');

        } else {
            $io->success('Best found currency exchange rate for '.$from.'/'.$to. ' is '.$rate);
        }

    }

}
