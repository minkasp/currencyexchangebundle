<?php

namespace Mkasp\CurrencyExchangeBundle\Command;

use Mkasp\CurrencyExchangeBundle\Repository\ExchangeRateRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class CurrencyRatesCommand extends ContainerAwareCommand
{
    /** @var null | ExchangeRateRepository  */
    private $exchangeRepository = null;


    protected function configure()
    {
        $this
            ->setName('currency:rates')
            ->setDescription('Retrieves available currency rates for given exchange pair. currency:rates usd jpy')
            ->addArgument('currencyFrom', InputArgument::REQUIRED, 'Currency you\'re selling')
            ->addArgument('currencyTo', InputArgument::REQUIRED, 'Currency you\'re buying');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $this->exchangeRepository = $em
            ->getRepository('Mkasp\CurrencyExchangeBundle\Entity\ExchangeRate');

        $from = strtoupper($input->getArgument('currencyFrom'));
        $to = strtoupper($input->getArgument('currencyTo'));

        // retrieving rate value array
        $rates = $this->exchangeRepository->getCalculatedRates($from, $to);
        $rows = [];

        // constructing rows for the table
        foreach ($rates as $rate) {
            $rows[] = [$from, $to, $rate];
        }

        if (empty($rates)) {
            $output->writeln("No exchange rates found for ".$from."/".$to. ' currency pair.');
        } else {

            $output->writeln(count($rates).' result(s) found.');
            $table = new Table($output);
            $table->setHeaders(['CURRENCY FROM', 'CURRENCY TO', 'EXCHANGE RATE'])->setRows($rows)->render();
        }

    }

}
