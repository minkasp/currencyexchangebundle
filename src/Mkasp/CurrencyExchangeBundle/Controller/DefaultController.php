<?php

namespace Mkasp\CurrencyExchangeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $rate = $this->get('app.exchange_rate');
        $banks = $rate->getBanks();

        return $this->render('MkaspCurrencyExchangeBundle:Default:index.html.twig', [
            'banks' => $banks,
        ]);
    }

    /**
     * @param $from
     * @param $to
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($from, $to)
    {
        $service = $this->get('app.exchange_rate');
        $banks = $service->getBanks();
        $rate = $service->get($from, $to);


        return $this->render('MkaspCurrencyExchangeBundle:Default:show.html.twig', [
            'rate' => $rate[0][0],
            'banks' => $banks,
            'bestBank' => $rate[1][0]
        ]);

    }
}
