<?php

namespace Mkasp\CurrencyExchangeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class BankController
 * @package Mkasp\CurrencyExchangeBundle\Controller
 */
class BankController extends Controller
{
    /**
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($slug)
    {
        $bank = $this->getDoctrine()->getEntityManager()
                ->getRepository('Mkasp\CurrencyExchangeBundle\Entity\Bank')->findBy(['slug' => $slug]);
        $rates = null;

        if ($bank != null && !empty($bank)) {
            $rates = $bank[0]->getRates();
        }

        return $this->render('MkaspCurrencyExchangeBundle:Bank:index.html.twig', [
            'rates' => $rates,
            'bank' => $bank,
        ]);
    }
}
