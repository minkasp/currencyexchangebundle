<?php

namespace Mkasp\CurrencyExchangeBundle\Repository;

/**
 * BankRepository
 */

use Doctrine\ORM\EntityManager;
use Mkasp\CurrencyExchangeBundle\Entity\Bank;
use Mkasp\CurrencyExchangeBundle\Service\Providers\Helpers\Helper;

class BankRepository extends \Doctrine\ORM\EntityRepository
{
    /** cache time limit in hours  */
    const CACHE_EXPIRES_IN_HRS = 3;

    /** base currency against all other currencies in exchange rate table and weighed */
    const BASE_CURRENCY = "EUR";

    /**
     * @param int $hour
     * @return bool
     */
    public function hasCacheExpired($hour = self::CACHE_EXPIRES_IN_HRS, $providerCount = 0)
    {
        $count = count($this->findAll());
        $newProvider = $providerCount != 0 && $count < $providerCount;

        $results = $this->getExpiredCacheQuery($hour);
        return count($results) > 0 || $this->isEmpty() || $newProvider;
    }

    /**
     * @param int $hour
     * @return mixed
     */
    public function getExpiredCacheQuery($hour = self::CACHE_EXPIRES_IN_HRS)
    {
        return $this->createQueryBuilder('t')
            ->select()
            ->andWhere('t.updatedAt <= :date')
            ->setParameter(':date', $this->getDate($hour))
            ->getQuery()
            ->getResult();
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return count($this->createQueryBuilder('t')
            ->select()
            ->getQuery()
            ->getResult()) == 0;
    }

    /**
     * @param $name
     * @return bool
     */
    public function bankExists($name)
    {
        $result = $this->getBankByName($name);

        return $result != null;
    }

    /**
     * @param $name
     * @return array
     */
    public function getBankByName($name)
    {
        return $this->createQueryBuilder('t')
            ->select()
            ->andWhere('t.name = :name')
            ->setParameter(':name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $name
     * @return array
     */
    public function getBankByNameExpired($name, $hour = self::CACHE_EXPIRES_IN_HRS)
    {
        return $this->createQueryBuilder('t')
            ->select()
            ->andWhere('t.name = :name')
            ->andWhere('t.updatedAt <= :date')
            ->setParameter(':date', $this->getDate($hour))
            ->setParameter(':name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $hour
     * @return string
     */
    private function getDate($hour)
    {
        $date = new \DateTime();
        $date->modify('-'.$hour.' hour');

        return $date->format("Y-m-d H:i");
    }

    /**
     * @param EntityManager $em
     * @param $provider
     * @return array|Bank
     */
    public function save(EntityManager $em, $provider)
    {
        $exists = $this->getBankByName($provider->getName());
        $date = new \DateTime();

        // no bank found, creating new entry
        if (empty($exists))  {
            $entity = new Bank();
            $entity->setCreatedAt($date);
            $entity->setName($provider->getName());
            $entity->setUpdatedAt($date);
            $entity->setSlug(Helper::slugify($provider->getName()));

        // bank exists, check if cache expired
        } else {
            $entity = $this->getBankByNameExpired($provider->getName());

            if (!empty($entity)) {
                reset($entity);
                $entity->setUpdatedAt($date);

            } else {
                return false;
            }
        }

        $em->persist($entity);
        $em->flush();

        return $entity;
    }

}
