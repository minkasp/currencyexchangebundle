<?php

namespace Mkasp\CurrencyExchangeBundle\Repository;

use Doctrine\ORM\EntityManager;
use Mkasp\CurrencyExchangeBundle\Entity\ExchangeRate;
use Mkasp\CurrencyExchangeBundle\Entity\Bank;

/**
 * ExchangeRateRepository
 */
class ExchangeRateRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @param $bank
     * @param string $from
     * @param $to
     * @return array
     */
    public function getRate($bank, $from = BankRepository::BASE_CURRENCY, $to)
    {
        return $this->createQueryBuilder('t')
            ->select()
            ->andWhere('t.bankId = :bank')
            ->andWhere('t.currencyFrom = :from')
            ->andWhere('t.currencyTo = :to')
            ->setParameter(':bank', $bank)
            ->setParameter(':from', $from)
            ->setParameter(':to', $to)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $from
     * @param $to
     * @return array
     */
    public function getAllRates($from, $to)
    {
        return $this->createQueryBuilder('t')
            ->select()
            ->andWhere('t.currencyFrom = :from')
            ->andWhere('t.currencyTo = :to')
            ->setParameter(':from', $from)
            ->setParameter(':to', $to)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $bank
     * @param string $from
     * @param $to
     * @return array
     */
    public function getRateExpired($bank, $from = BankRepository::BASE_CURRENCY, $to)
    {
        return $this->createQueryBuilder('t')
            ->select()
            ->andWhere('t.bankId <= :bank')
            ->andWhere('t.currencyFrom <= :from')
            ->andWhere('t.currencyTo <= :to')
            ->andWhere('t.updatedAt <= :date')
            ->setParameter(':date', $this->getDate(BankRepository::CACHE_EXPIRES_IN_HRS))
            ->setParameter(':bank', $bank)
            ->setParameter(':from', $from)
            ->setParameter(':to', $to)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $hour
     * @return string
     */
    private function getDate($hour)
    {
        $date = new \DateTime();
        $date->modify('-'.$hour.' hour');
        $date->format("Y-m-d H:i");

        return $date;
    }

    /**
     * @param EntityManager $em
     * @param Bank $bank
     * @param $rate
     * @param $currencyTo
     * @return array|bool|ExchangeRate
     */
    public function save(EntityManager $em, Bank $bank, $rate, $currencyTo)
    {
        $exists = $this->getRate($bank->getId(), BankRepository::BASE_CURRENCY, $currencyTo);
        $date = new \DateTime();

        if ($exists == null) {
            $entity = new ExchangeRate();
            $entity->setCreatedAt($date);
            $entity->setUpdatedAt($date);
            $entity->setCurrencyFrom(BankRepository::BASE_CURRENCY);
            $entity->setCurrencyTo($currencyTo);
            $entity->setBank($bank);
            $entity->setBankId($bank->getId());
            $entity->setRate((float) $rate);

        } else {
            $entity = $this->getRateExpired($bank->getId(), null, $currencyTo);

            if (!empty($entity)) {
                reset($entity);
                $entity->setRate((float) $rate);
                $entity->setUpdatedAt($date);

            } else {
                return false;
            }
        }

        $em->persist($entity);
        $em->flush();

        return $entity;
    }

    /**
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getBestRate($from, $to)
    {
        $array = $this->getCalculatedRates($from, $to);
        sort($array[0]);

        return count($array) ?  [reset($array), $array[1]] : null;
    }

    /**
     * @param $from
     * @param $to
     * @return array
     */
    public function getCalculatedRates($from, $to)
    {
        if ($from == BankRepository::BASE_CURRENCY && $to != $from) {
            $fromRates = $this->getAllRates(BankRepository::BASE_CURRENCY, $to);

            foreach($fromRates as $rate) {
                $rates[] = $rate->getRate();
                $banks[] = $rate->getBank();
            }

        } elseif ($to == BankRepository::BASE_CURRENCY && $from != $to) {
            $toRates = $this->getAllRates(BankRepository::BASE_CURRENCY, $from);

            foreach ($toRates as $rate) {
                $rates[] = round(1 / $rate->getRate(), 4);
                $banks[] = $rate->getBank();
            }

        } else {
            $fromRates = $this->getAllRates(BankRepository::BASE_CURRENCY, $from);
            $toRates = $this->getAllRates(BankRepository::BASE_CURRENCY, $to);
            $result =  $this->getRatesArray($fromRates, $toRates);

            $rates = $result[0];
            $banks = $result[1];
        }

        return [$rates, $banks];
    }

    /**
     * @param $fromRates
     * @param $toRates
     * @return array
     */
    private function getRatesArray($fromRates, $toRates)
    {
        $rates = [];

        foreach ($fromRates as $from) {
            foreach ($toRates as $to) {
                $rates[] = (float) round($to->getRate() / $from->getRate() , 4);
                $banks[] = $from->getBank();
            }
        }

        return [$rates, $banks];
    }
}
