<?php

namespace Mkasp\CurrencyExchangeBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Yaml;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class MkaspCurrencyExchangeExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $cacheConfig = __DIR__ . '/../Resources/config/cache.yml';
        $configs = array_merge(Yaml::parse(file_get_contents($cacheConfig)), $configs);

        $configuration  = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $this->validateConfig($configs, $config);

        $options = array();
        foreach ($config['cache'] as $key => $value) {
            $options[$key] = $value;
        };

        $container->setParameter('exchange_rate_cache.cache.defaults', $options);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * Checks whether there are any missing caching settings in the bundle configuration
     * @param array $configs
     * @throws \RuntimeException
     */
    private function validateConfig(array $configs, array $config)
    {
        if (!isset($configs['mkasp_currency_exchange'])) {
            throw new \RuntimeException('configuration mkasp_currency_exchange is missing.');
        }

        if (!isset($config['cache'])) {
            throw new \RuntimeException('configuration mkasp_currency_exchange.cache is missing.');
        }

        if (!isset($config['cache']['defaults'])) {
            throw new \RuntimeException('configuration mkasp_currency_exchange.cache.defaults is missing.');
        }
    }
}
