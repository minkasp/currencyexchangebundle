<?php

namespace Mkasp\CurrencyExchangeBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Mkasp\CurrencyExchangeBundle\Service\Cache\ExchangeRateCache;

/**
 * Class ExchangeRateService
 * @package Mkasp\CurrencyExchangeBundle\Service
 */
class ExchangeRateService extends ExchangeRateCache implements ExchangeRateServiceInterface
{
    /**
     * ExchangeRateService constructor.
     * @param ContainerInterface $container
     * @param array $serviceProviders
     */
    public function __construct(ContainerInterface $container, array $serviceProviders)
    {
        $this->container = $container;
        $this->serviceProviders = $serviceProviders;

        parent::__construct($container);

        $this->cache();
    }

    /**
     * Retrieves best currency exchange rate for specified currency pair
     * @param $from
     * @param $to
     * @return float
     */
    public function get($from, $to)
    {
        return $this->findBest(strtoupper($from), strtoupper($to));
    }

    /**
     * Retrieves bank names
     * @return array
     */
    public function getBanks()
    {
        return $this->serviceProviders;
    }

    /**
     * @return bool
     */
    public function isCached()
    {
        return !$this->bankRepository->hasCacheExpired();
    }

    /**
     * Retrieves best currency exchange rate
     * @param $from
     * @param $to
     * @return array|mixed
     */
    protected function findBest($from, $to)
    {
        // the search for the best provider
        if (!empty($this->serviceProviders)) {

            if ($this->isCached()) {

                // retrieve from caching
                return $this->exchangeRepository
                    ->getBestRate($from, $to);

            } else {
                $this->cache();

                return $this->exchangeRepository
                    ->getBestRate($from, $to);
            }
        }

        return [];
    }

    /**
     * @param $from
     * @param $to
     * @return array
     */
    protected function getAllRates($from, $to)
    {
        $rates = [];

        foreach ($this->serviceProviders as $provider) {
            $rates[] = $provider->getExchangeRate($from, $to);
        }

        return $rates;
    }
}