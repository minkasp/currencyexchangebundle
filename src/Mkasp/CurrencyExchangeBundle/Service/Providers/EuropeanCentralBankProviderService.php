<?php

namespace Mkasp\CurrencyExchangeBundle\Service\Providers;

/**
 * Class EuropeanCentralBankProviderService
 * This service class directly calls to an API end points to retrieve relevant data
 * @package Mkasp\CurrencyExchangeBundle\Service\ProvidersW
 */
class EuropeanCentralBankProviderService extends ProviderService implements ProviderInterface
{
    /** @var string request url */
    protected $latestUrl = "http://api.fixer.io/latest";


    /** @var string bank name */
    protected $name = 'European Central Bank';

    /**
     * Returns bank name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $from base currency
     * @param $to conversion currency
     * @return array bank name and rate key-value pair
     */
    public function getExchangeRate($from, $to)
    {
        $rates = $this->getAllRates($from);

        if (isset($rates->$to)) {
            return [$this->name => $rates->$to];
        }

        return [];
    }

    /**
     * @param string $from
     * @return array all rates for given base currency
     */
    public function getAllRates($from = 'EUR')
    {
        try {
            $request = $this->client->get($this->latestUrl.'?base='.$from);

            if ($request->getStatusCode() == 200) {
                $content = json_decode($request->getContent());
                return $content->rates;
            }

        } catch (\ErrorException $e) {
            //TODO: add logger
        }
    }
}