<?php

namespace Mkasp\CurrencyExchangeBundle\Service\Providers;

use Mkasp\CurrencyExchangeBundle\Repository\BankRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OpenExchangeRatesProviderService
 * this service class directly calls to an API end point to retrieve relevant data
 * @package Mkasp\CurrencyExchangeBundle\Service\Providers
 */
class OpenExchangeRatesProviderService extends ProviderService implements ProviderInterface
{
    /** @var string request url */
    protected $latestUrl = "http://openexchangerates.org/api/latest.json?";

    /** @var string bank name */
    protected $name = 'Open Exchange API data';

    /** @var string api access key */
    protected $accessKey = null;

    /**
     * CurrencyLayerProviderService constructor.
     * @param ContainerInterface $container
     * @param $accessKey
     */
    public function __construct(ContainerInterface $container, $accessKey)
    {
        $this->accessKey = $accessKey;
        parent::__construct($container);
    }

    /**
     * Returns bank name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $from base currency
     * @param $to conversion currency
     * @return array bank name and rate key-value pair
     */
    public function getExchangeRate($from, $to)
    {
        $rates = $this->getAllRates($from);

        if (isset($rates->$to)) {
            return [$this->name => $rates->$to];
        }

        return [];
    }

    /**
     * @param string $from
     * @return mixed
     */
    public function getAllRates($from = 'EUR')
    {
        try {
            $url = $this->latestUrl.'app_id='.$this->accessKey;
            $request = $this->client->get($url);

            if ($request->getStatusCode() == 200) {
                $content = json_decode($request->getContent());

                // source currency switching not enabled, let's calculate it ourselves
                return $this->getCalculatedRates((array) $content->rates);
            }

        } catch (\ErrorException $e) {
            //TODO: add logger
        }

        return null;
    }

    /**
     * Constructing EUR -> currencies conversion collection
     * since this end point returns currencies for USD as base currency only
     *
     * @param array $rates
     * @return array
     */
    private function getCalculatedRates(array $rates)
    {
        $ratesArray = [];

        if (isset($rates['EUR'])) {
            $eur = (float) $rates['EUR'];

            foreach($rates as $key => $rate) {
                if ($key != 'USD' && $key != BankRepository::BASE_CURRENCY) {
                    $ratesArray[$key] = (float) round($rate / $eur, 8);
                }
            }
        }

        return (object) $ratesArray;
    }

}