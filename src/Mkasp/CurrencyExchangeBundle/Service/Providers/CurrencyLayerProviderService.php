<?php

namespace Mkasp\CurrencyExchangeBundle\Service\Providers;

/**
 * Class CurrencyLayerProviderService
 * this service class directly calls to an API end point to retrieve relevant data
 * @package Mkasp\CurrencyExchangeBundle\Service\Providers
 */

use Mkasp\CurrencyExchangeBundle\Repository\BankRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CurrencyLayerProviderService extends ProviderService implements ProviderInterface
{
    /** @var string request url */
    protected $latestUrl = "http://apilayer.net/api/";

    /** @var string bank name */
    protected $name = 'Currency Layer API data';

    /** @var string api access key */
    protected $accessKey = null;

    /**
     * CurrencyLayerProviderService constructor.
     * @param ContainerInterface $container
     * @param $accessKey
     */
    public function __construct(ContainerInterface $container, $accessKey)
    {
        $this->accessKey = $accessKey;
        parent::__construct($container);
    }

    /**
     * Returns bank name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $from base currency
     * @param $to conversion currency
     * @return array bank name and rate key-value pair
     */
    public function getExchangeRate($from, $to)
    {
        $rates = $this->getAllRates($from);

        if (isset($rates->$to)) {
            return [$this->name => $rates->$to];
        }

        return [];
    }

    /**
     * @param string $from
     * @return mixed
     */
    public function getAllRates($from = 'EUR')
    {
        try {
            $url = $this->latestUrl.'live?access_key='.$this->accessKey;
            $request = $this->client->get($url);

            if ($request->getStatusCode() == 200) {
                $content = json_decode($request->getContent());

                // source currency switching not enabled, let's calculate it ourselves
                return $this->getCalculatedRates((array) $content->quotes);
            }

        } catch (\ErrorException $e) {
            //TODO: add logger
        }
    }

    /**
     * @param array $rates
     * @return array
     */
    private function getCalculatedRates(array $rates)
    {
        $ratesArray = [];

        if (isset($rates['USDEUR'])) {
            $eur = (float) $rates['USDEUR'];

            foreach($rates as $key => $rate) {
                $currency = explode('USD', $key)[1];

                if ($currency != 'USD' && $currency != BankRepository::BASE_CURRENCY) {
                    $ratesArray[$currency] = (float) round($rate / $eur, 8);
                }
            }
        }

        return (object) $ratesArray;
    }

}