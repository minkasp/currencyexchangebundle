<?php

namespace Mkasp\CurrencyExchangeBundle\Service\Providers;

/**
 * Interface ProviderInterface
 * @package Mkasp\CurrencyExchangeBundle\Service\Providers
 */
interface ProviderInterface
{
    public function getExchangeRate($from, $to);

    public function getName();

    public function getAllRates($from = 'EUR');

}