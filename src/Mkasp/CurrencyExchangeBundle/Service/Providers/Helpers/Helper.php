<?php

namespace Mkasp\CurrencyExchangeBundle\Service\Providers\Helpers;

/**
 * Class Helper
 * @package Mkasp\CurrencyExchangeBundle\Service\Providers
 *
 * Helper class for various algorithms that are not present in symfony core.
 */
class Helper
{
    /**
     * Creates a friendly url
     * @param $str
     * @return string
     */
    public static function slugify($str)
    {
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);

        return rtrim($str, '-');
    }

    /**
     * @param $array
     * @return array
     */
    public static function array_flatten($array) {
        $return = array();

        foreach ($array as $key => $value) {
            if (is_array($value)){
                $return = array_merge($return, self::array_flatten($value));
            } else {
                $return[$key] = $value;
            }
        }

        return $return;
    }

}