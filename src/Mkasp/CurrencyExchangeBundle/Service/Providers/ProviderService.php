<?php

namespace Mkasp\CurrencyExchangeBundle\Service\Providers;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Mkasp\CurrencyExchangeBundle\Service\Providers\Helpers\Helper;

/**
 * Class ProviderService
 * @package Mkasp\CurrencyExchangeBundle\Service\Providers
 */
class ProviderService extends Helper
{
    /** @var null | object rest request client object */
    protected $client = null;

    /** @var null  */
    protected $slug = null;

    /** @var null  */
    protected $name = null;

    /**
     * ProviderService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->client = $container->get('circle.restclient');
        $this->setSlug();
    }

    /**
     * Setter method for slug
     */
    protected function setSlug()
    {
        $this->slug = Helper::slugify($this->getName());
    }

    /**
     * slugified bank name
     * @return null | string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}