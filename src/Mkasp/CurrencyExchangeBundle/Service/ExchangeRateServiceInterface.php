<?php

namespace Mkasp\CurrencyExchangeBundle\Service;

interface ExchangeRateServiceInterface
{
    /**
     * @param $from
     * @param $to
     * @return float
     */
    public function get($from, $to);

    /**
     * @return boolean
     */
    public function isCached();

    /**
     * @return mixed
     */
    public function getBanks();

}