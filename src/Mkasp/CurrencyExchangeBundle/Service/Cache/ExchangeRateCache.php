<?php

namespace Mkasp\CurrencyExchangeBundle\Service\Cache;

use Doctrine\Common\Collections\ArrayCollection;
use Mkasp\CurrencyExchangeBundle\Entity\ExchangeRate;
use Mkasp\CurrencyExchangeBundle\Repository\BankRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ExchangeRateCache
 * This object is coupled with ExchangeRateService
 */
class ExchangeRateCache
{
    /** @var ContainerInterface container object */
    protected $container;

    /** @var array exchange service provider array */
    protected $serviceProviders = [];

    /** Database caching */
    const CACHE_IN_DB = 0;

    /**
     * File caching
     * TODO: not implemented yet
     **/
    const CACHE_IN_FILE = 1;

    /** @var int exchange currency data caching type  */
    protected $cacheType = null;

    /** @var null  */
    protected $bankRepository = null;

    /** @var null  */
    protected $exchangeRepository = null;

    /** @var null  */
    protected $entityManager = null;

    /**
     * ExchangeRateCache constructor.
     * @param ContainerInterface $container
     * @throws \Exception
     */
    public function __construct(ContainerInterface $container)
    {
        // set caching
        $params = $container->getParameter('exchange_rate_cache.cache.defaults');

        $this->entityManager = $container->get('doctrine')->getEntityManager();

        // loading repositories for caching
        $this->bankRepository = $this->entityManager
            ->getRepository('Mkasp\CurrencyExchangeBundle\Entity\Bank');

        $this->exchangeRepository = $this->entityManager
            ->getRepository('Mkasp\CurrencyExchangeBundle\Entity\ExchangeRate');

        if (isset($params['type'])) {
            $this->cacheType = $params['type'];

        } else if (isset($params['defaults']['type'])) {
            $this->cacheType = $params['defaults']['type'];

        } else {
            throw new \Exception('Cache type not found. Check your cache configurations');
        }

        // checking if caching required
        $this->cache();
    }

    /**
     * @return null
     */
    public function cache()
    {
        $expired = $this->bankRepository->hasCacheExpired(BankRepository::CACHE_EXPIRES_IN_HRS,
            count($this->serviceProviders));

        if (!is_null($this->cacheType) && $expired) {
            $this->selectCachingByType($this->cacheType);
        }

        return null;
    }

    /**
     * @param $type
     */
    protected function selectCachingByType($type)
    {
        switch ($type) {
            case self::CACHE_IN_DB:
                $this->storeInDB();
            default:
                $this->storeInDB();
        }
    }

    /**
     * Data storage in database
     */
    protected function storeInDB()
    {
        foreach ($this->serviceProviders as $provider) {
            $rateCollection = [];
            $rates = $provider->getAllRates();
            $bankId = null;
            $newEntity = !$this->bankRepository->bankExists($provider->getName());

            if ($newEntity) {
                $entity = $this->bankRepository->save($this->entityManager, $provider);
                $bankId = $entity->getId();
            } else {
                $entity = $this->bankRepository->getBankByName($provider->getName());
                //updating bank entity to let service know cache not needed now
                $this->bankRepository->save($this->entityManager, $provider);

                if ($entity) {
                    $bankId = $entity->getId();
                }
            }

            // updating exchange rate entities
            foreach ($rates as $currency => $rate) {
                if ($bankId != null) {
                    $result = $this->exchangeRepository->save($this->entityManager, $entity, $rate, $currency);

                    if ($result instanceof ExchangeRate) {
                        $rateCollection[] = $result;
                    }
                }
            }

            if (!empty($rateCollection)) {
                $collection = new ArrayCollection($rateCollection);
                $entity->setRates($collection);
                $this->entityManager->flush();
            }
        }

    }

}